package com.formacionbdi.springboot.app.usuarios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

//@SpringBootApplication(scanBasePackages = "com.formacionbdi.springboot.app.usuarios.commons.models.entity")
@ComponentScan({"com.formacionbdi.springboot.app.usuarios.commons.models.entity"})
@SpringBootApplication
@Import(RepositoryConfig.class)
public class SpringbootServicioUsuariosApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServicioUsuariosApplication.class, args);
	}

}
